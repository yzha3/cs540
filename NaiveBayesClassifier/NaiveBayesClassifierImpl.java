/**
 * Your implementation of a naive bayes classifier. Please implement all four methods.
 */
import java.util.Map;
import java.util.HashMap;

public class NaiveBayesClassifierImpl implements NaiveBayesClassifier {
	/**
	 * Trains the classifier with the provided training data and vocabulary size
	 */
	private double delta=0.00001,HAM_WORD=0,SPAM_WORD=0;
	private int HAM=0,SPAM=0,Label_total;
	
	private Map<String,Integer> HAM_Word=new HashMap<String,Integer>();
	private Map<String,Integer> SPAM_Word=new HashMap<String,Integer>();
	
	@Override
	public void train(Instance[] trainingData, int v) {
		for (Instance x:trainingData)
			if (x.label==Label.HAM) 
			{
				HAM++;
				for (String y:x.words)
				{
					if (!HAM_Word.containsKey(y)) HAM_Word.put(y,1);
					else HAM_Word.put(y,1+HAM_Word.get(y));
					HAM_WORD++;
				}
			}
			else 
			{
				SPAM++;
				for (String y:x.words)
				{
					if (!SPAM_Word.containsKey(y)) SPAM_Word.put(y,1);
					else SPAM_Word.put(y,1+SPAM_Word.get(y));
					SPAM_WORD++;
				}
			}
		Label_total=HAM+SPAM;
		HAM_WORD+=v*delta;
		SPAM_WORD+=v*delta;
	}

	/**
	 * Returns the prior probability of the label parameter, i.e. P(SPAM) or P(HAM)
	 */
	@Override
	public double p_l(Label label) {
		if (label==Label.HAM) return Math.log(HAM*1.0/Label_total);
		return Math.log(SPAM*1.0/Label_total);
	}

	/**
	 * Returns the smoothed conditional probability of the word given the label,
	 * i.e. P(word|SPAM) or P(word|HAM)
	 */
	@Override
	public double p_w_given_l(String word, Label label) {
		double count=delta;
		if (label==Label.HAM)
		{
			if (HAM_Word.containsKey(word)) count+=HAM_Word.get(word);
			return Math.log(count/HAM_WORD);
		}
		else
		{
			if (SPAM_Word.containsKey(word)) count+=SPAM_Word.get(word);
			return Math.log(count/SPAM_WORD);
		}
	}
	
	/**
	 * Classifies an array of words as either SPAM or HAM. 
	 */
	@Override
	public ClassifyResult classify(String[] words) {
		ClassifyResult result=new ClassifyResult();

		result.log_prob_spam=p_l(Label.SPAM);
		result.log_prob_ham=p_l(Label.HAM);
		for (String x: words)
		{
			result.log_prob_ham+=p_w_given_l(x,Label.HAM);
			result.log_prob_spam+=p_w_given_l(x,Label.SPAM);
		}

		if (result.log_prob_ham>=result.log_prob_spam) result.label=Label.HAM;
		else result.label=Label.SPAM;

		return result;
	}
}
