/*************************************************************
	>File Name: KMeans.java
	>Author: Yue
	>Created Time: Fri 12 Sep 2014 12:11:55 PM CDT
	>Description: K-means cluster algorithm
*************************************************************/
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

public class KMeans
{
	public KMeansResult cluster(double[][] centroids,double[][] instances,double threshold) 
	{ 
		KMeansResult result=new KMeansResult();
		int[] count=new int[centroids.length];
		double stop=100.0;

		double[][] distance=new double[instances.length][centroids.length];
		ArrayList<Double> distortion=new ArrayList<Double>();
		result.clusterAssignment=new int [instances.length];
		result.centroids=new double[centroids.length][centroids[0].length];
		for (int i=0;i<centroids.length;i++)
			for (int j=0;j<centroids[0].length;j++)
				result.centroids[i][j]=centroids[i][j];

		Calc_distance(result.centroids,instances,result.clusterAssignment,distance);

		while (stop>threshold)
		{
			Assignment(result.clusterAssignment,distance,count);
			Check_orphaned(distance,result.clusterAssignment,result.centroids,instances,count);
			Average(instances,result.clusterAssignment,result.centroids,count);
			distortion.add(Calc_distance(result.centroids,instances,result.clusterAssignment,distance));
			if (distortion.size()>1) stop=(distortion.get(distortion.size()-2)-distortion.get(distortion.size()-1))/distortion.get(distortion.size()-2);
		} 
		result.distortionIterations=new double[distortion.size()];
		for (int i=0;i<distortion.size();i++)
			result.distortionIterations[i]=distortion.get(i);
		return result;
	}

	private double Calc_distance(double[][]centroids,double[][] instances,int[] clusterAssignment,double[][] distance)
	{
		double distortion=0;
		for (int i=0;i<instances.length;i++)
		{
			for (int j=0;j<centroids.length;j++)
			{
				distance[i][j]=0;
				for (int k=0;k<centroids[j].length;k++)
					distance[i][j]+=(centroids[j][k]-instances[i][k])*(centroids[j][k]-instances[i][k]);
			}
			distortion+=distance[i][clusterAssignment[i]];
		}
		return distortion;
	 }
	private void Assignment(int[] clusterAssignment,double[][]distance,int[] count)
	{
			for (int i=0;i<count.length;i++) count[i]=0;

			for (int i=0;i<clusterAssignment.length;i++)
			{
				clusterAssignment[i]=0;
				for (int j=0;j<distance[0].length;j++)
					if (distance[i][j]<distance[i][clusterAssignment[i]]) clusterAssignment[i]=j;
				count[clusterAssignment[i]]++;
			}
	}
	private void Check_orphaned(double[][] distance,int[] clusterAssignment,double[][] centroids,double[][] instances,int[] count)
	{
		int i=0,far_point,j;
		double max=0;
		while (i<count.length)
		{
			if (count[i]!=0) i++;
			else
			{
				far_point=0;
				for (j=0;j<distance.length;j++) 
					if ((distance[j][clusterAssignment[j]]>distance[far_point][clusterAssignment[far_point]])) far_point=j;
				clusterAssignment[far_point]=i;
				for (j=0;j<centroids[0].length;j++) centroids[i][j]=instances[far_point][j];
				Calc_distance(centroids,instances,clusterAssignment,distance);
				Assignment(clusterAssignment,distance,count);
				i=0;
			}
		}
	}
	private void Average(double[][] instances,int[] assignment,double[][] centroids,int[] count)
	{
		for (int i=0;i<count.length;i++)
			for (int j=0;j<centroids[i].length;j++) centroids[i][j]=0.0;

		for (int i=0;i<instances.length;i++)
			for (int j=0;j<centroids[0].length;j++) centroids[assignment[i]][j]+=instances[i][j];

		for (int i=0;i<count.length;i++)
			for (int j=0;j<centroids[0].length;j++) centroids[i][j]/=count[i];
	}
}
