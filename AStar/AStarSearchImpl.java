import java.util.HashMap;
import java.util.PriorityQueue;

public class AStarSearchImpl implements AStarSearch {
	private int[][] index={{0,2,6,11,15,20,22},{1,3,8,12,17,21,23},{10,9,8,7,6,5,4},{19,18,17,16,15,14,13}};

	private PriorityQueue<State> OPEN=new PriorityQueue<State>(100,State.comparator);
	//Config stores all config in OPEN and CLOSE
	private HashMap<String,State> Config=new HashMap<String,State>();

	@Override	
	public SearchResult search(String initConfig, int modeFlag) {

		State parent=new State(initConfig,0,getHeuristicCost(initConfig,modeFlag),"");
		Config.put(initConfig,parent);

		State Child,temp;
		int popout=0,realCost;
		String temp_config;

		while (parent!=null)
		{
			//find state with smallest f(n)
			popout++;

			//Find goal
			if (checkGoal(parent.config))
				return new SearchResult(parent.config,parent.opSequence,popout);
	
			realCost=parent.realCost+1;
			//Generate Child
			for (char x='A';x<'I';x++)
			{
				temp_config=move(parent.config,x);
				if (Config.containsKey(temp_config)&& Config.get(temp_config).realCost<=realCost)
					continue;

				Child=new State(temp_config,realCost,getHeuristicCost(temp_config,modeFlag),parent.opSequence+x);
				OPEN.remove(Config.get(temp_config));
				Config.put(temp_config,Child);
				OPEN.add(Child);
			}
			parent=OPEN.poll();

		}
		return null;
	}

	@Override
	public boolean checkGoal(String config) {
		char goal=config.charAt(6);
		for (int i=0;i<2;i++)
		{
			for (int j=2;j<5;j++)
				if (config.charAt(index[i][j])!=goal) return false;
			if (config.charAt(index[i+2][3])!=goal) return false;
		}
		return true;
	}

	@Override
	public String move(String config, char op) {
		int line,direction;
		int OP=op-'A';
	
		line=(OP%4)^(OP/4);
		if (OP<4) direction=1;
		else direction=-1;

		char [] temp=config.toCharArray();
		char swap=temp[index[line][0]];
		int i=0;
		for (int j=0;j<6;j++)
		{
			temp[index[line][i]]=temp[index[line][(i+7+direction)%7]];
			i=(i+7+direction)%7;
		}
		temp[index[line][i]]=swap;

		return new String(temp);
	}

	@Override
	public int getHeuristicCost(String config, int modeFlag) {		
		int []temp={0,0,0};
		int small;
		switch (modeFlag)
		{
			default:
				return 0;
			case 1:
				for (int i=0;i<4;i++)
					for (int j=5;j<9;j++)
						temp[config.charAt(index[i][j%7])-'1']++;
				small=temp[0];
				for (int i=1;i<3;i++)
					if (small>temp[i]) small=temp[i];
				return small;
			case 3:
				int [] count={0,0,0};
				for (int i=0;i<4;i++)
				{
					count[0]=0;count[1]=0;count[2]=0;
					for (int j=5;j<9;j++)
					{
						temp[config.charAt(index[i][j%7])-'1']++;
						count[config.charAt(index[i][j%7])-'1']++;
					}
					for (int j=2;j<5;j++)
						count[config.charAt(index[i][j])-'1']++;
					for (int j=0;j<3;j++)
						if (count[j]>3) temp[j]+=count[j]-3;
				}
				small=temp[0];
				for (int i=1;i<3;i++)
					if (small>temp[i]) small=temp[i];
				return small;
		}
	}
}
