import java.util.*;

public class PlayerImpl implements Player {
	// Identifies the player
	private int name = 0;
	int n = 0;

	// Constructor
	public PlayerImpl(int name, int n) {
		this.name = 0;
		this.n = n;
	}

	// Function to find possible successors
	@Override
	public ArrayList<Integer> generateSuccessors(int lastMove, int[] crossedList) {
		ArrayList<Integer> nextMove=new ArrayList<Integer>();
		if (lastMove==-1)
			for (int x=(n-1)/4*2;x>1;x-=2)
					nextMove.add(x);
		else
			for (int x=n;x>0;x--)
				if ((x % lastMove ==0 || lastMove % x ==0) && crossedList[x]==0)
					nextMove.add(x);
		return nextMove;
	}

	// The max value function
	@Override
	public int max_value(GameState s) {
		ArrayList<Integer> Next=generateSuccessors(s.lastMove,s.crossedList);
		s.bestMove=-1;
		if (!Next.isEmpty()){
			GameState succ;
			for (Integer x:Next)
			{
				succ=new GameState(s.crossedList, x);
				succ.crossedList[x]=1;
				if(min_value(succ)==1)
				{
					s.bestMove=x;
					return 1;
				}
			}
			s.bestMove=Next.get(0);
		}
		return -1;
	}

	// The min value function
	@Override
	public int min_value(GameState s) {
		ArrayList <Integer> Next=generateSuccessors(s.lastMove, s.crossedList);
		if (!Next.isEmpty()){
			GameState succ;
			for (Integer x:Next)
			{
				succ= new GameState(s.crossedList, x);
				succ.crossedList[x]=1;
				if (max_value(succ)==-1)
				{
					s.bestMove=x;
					return -1;
				}
			}
			s.bestMove=Next.get(0);
		}
		return 1;
	}

	// Function to find the next best move
	@Override
	public int move(int lastMove, int[] crossedList) {
		GameState s=new GameState(crossedList, lastMove);
		max_value(s);
		return s.bestMove;
	}
}
