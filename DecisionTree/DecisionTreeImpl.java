import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Fill in the implementation details of the class DecisionTree using this file.
 * Any methods or secondary classes that you want are fine but we will only
 * interact with those methods in the DecisionTree framework.
 * 
 * You must add code for the 5 methods specified below.
 * 
 * See DecisionTree for a description of default methods.
 */
public class DecisionTreeImpl extends DecisionTree {
	private DecTreeNode root;
	private List<String> labels; // ordered list of class labels
	private List<String> attributes; // ordered list of attributes
	private Map<String, List<String>> attributeValues; // map to ordered
														// discrete values taken
														// by attributes

	DecisionTreeImpl() {
	}

	DecisionTreeImpl(DataSet train) {

		this.labels = train.labels;
		this.attributes = train.attributes;
		this.attributeValues = train.attributeValues;

		int [] Attribute_dimension=new int [this.attributes.size()];
		for (int i=0;i<Attribute_dimension.length;i++) 
			Attribute_dimension[i]=this.attributeValues.get(this.attributes.get(i)).size();
		
		this.root=BuildTree(Attribute_dimension,this.labels.size(),train.instances,-1);
	}

	DecisionTreeImpl(DataSet train, DataSet tune) {

		this.labels = train.labels;
		this.attributes = train.attributes;
		this.attributeValues = train.attributeValues;
		
		int [] Attribute_dimension=new int [train.attributes.size()];
		for (int i=0;i<Attribute_dimension.length;i++) 
			Attribute_dimension[i]=train.attributeValues.get(train.attributes.get(i)).size();
		
		this.root=Prune(tune.instances,BuildTree(Attribute_dimension,this.labels.size(),train.instances,-1));
	}

	public String classify(Instance instance) {
		return this.labels.get(classify(instance,this.root));
	}
	public void print() {

		printTreeNode(root, null, 0);
	}
	
	public void printTreeNode(DecTreeNode p, DecTreeNode parent, int k) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < k; i++) {
			sb.append("    ");
		}
		String value;
		if (parent == null) {
			value = "ROOT";
		} else{
			String parentAttribute = attributes.get(parent.attribute);
			value = attributeValues.get(parentAttribute).get(p.parentAttributeValue);
		}
		sb.append(value);
		if (p.terminal) {
			sb.append(" (" + labels.get(p.label) + ")");
			System.out.println(sb.toString());
		} else {
			sb.append(" {" + attributes.get(p.attribute) + "?}");
			System.out.println(sb.toString());
			for(DecTreeNode child: p.children) {
				printTreeNode(child, p, k+1);
			}
		}
	}

	public void rootInfoGain(DataSet train) {

		
		this.labels = train.labels;
		this.attributes = train.attributes;
		this.attributeValues = train.attributeValues;
		
		double total_Entropy=Entropy(train.instances,this.labels.size());
		
		for (int i=0;i<train.attributes.size();i++)
			System.out.format("%s %.5f\n",train.attributes.get(i),total_Entropy-Entropy(train.instances,i,this.labels.size(),this.attributeValues.get(this.attributes.get(i)).size()));

	}
	/* Add below functions:
	 *
	 *BuildTree(int[] , int , List<Instance> , int); Base on input instances, build a decision tree and return the root
	 *
	 *Prune(List<Instance> , DecTreeNode); Base on input tune instances and a root of a tree, prune the tree and return the root of new tree
	 *
	 *Accuracy(List<INstance> , DecTreeNode); Base on input test instances and a node, give the classify accuracy
	 * 
	 *classify(Instance ); Override function, return the int value of label
	 *
	 *Entropy(Instance , int); Don't divide the instances and calculate the entropy
	 *
	 *Entropy(Instance , int, int, int); Override function, divide the instances based on the designated attribute and give the entropy
	 */

	private DecTreeNode BuildTree(int[] Attribute_dimension,int Label_size,List<Instance> instance,int parentValue)
	{
		double small_entropy=Math.log(Label_size)/Math.log(2)+1;//The largest possible entropy and then plus 1
		int BestAttribute=-1;
		double temp_entropy;
		int major_label=0;
		int major_number=0;
		int []count=new int [Label_size];

		int [] copy_Attribute_dimension=Attribute_dimension.clone();
		List<Instance> temp_instance=new ArrayList<Instance>();
		
		for (int i=0;i<Attribute_dimension.length;i++)//find the "best question"
			if (Attribute_dimension[i]>0)
			{
				temp_entropy=Entropy(instance,i,Label_size,Attribute_dimension[i]);
				if (temp_entropy<small_entropy)
				{
					small_entropy=temp_entropy;
					BestAttribute=i;
				}
			}

		for (Instance x:instance) count[x.label]++;//find major label
		for (int i=0;i<count.length;i++) 
			if (count[i]>major_number) 
			{
				major_label=i;
				major_number=count[i];
			}
		if (BestAttribute<0||Entropy(instance,Label_size)==0)//No abailable attribute or all instances have the same label
			return new DecTreeNode(major_label,BestAttribute,parentValue,true);

		DecTreeNode temp=new DecTreeNode(major_label,BestAttribute,parentValue,false);//this node has children

		copy_Attribute_dimension[BestAttribute]=0;
		for (int i=0;i<Attribute_dimension[BestAttribute];i++)
		{
			temp_instance.clear();
			for (Instance x:instance)
				if (x.attributes.get(BestAttribute)==i) temp_instance.add(x);
			if (temp_instance.size()>0)
				temp.addChild(BuildTree(copy_Attribute_dimension,Label_size,temp_instance,i));//build the children for this node
			else 
				temp.addChild(new DecTreeNode(major_label,BestAttribute,i,true));//no instances belong to this child
		}
		
		return temp;
	}

	private DecTreeNode Prune(List<Instance> instance,DecTreeNode root)
	{
		DecTreeNode PruneNode,_Prune;
		List<DecTreeNode> BFS=new ArrayList<DecTreeNode> ();
		double beforeprune,afterprune;
		beforeprune=Accuracy(instance,root);
		do{
			_Prune=null;
			if (root.terminal==false) BFS.add(root);
			while (BFS.size()>0)
			{
				PruneNode=BFS.get(0);
				BFS.remove(0);
				for (DecTreeNode x:PruneNode.children)
					if (x.terminal==false) BFS.add(x);

				PruneNode.terminal=true;
				afterprune=Accuracy(instance,root);
				PruneNode.terminal=false;

				if (afterprune>=beforeprune)
				{
					_Prune=PruneNode;
					beforeprune=afterprune;
				}	
			}
			if (_Prune!=null)
			{
				_Prune.terminal=true;
				_Prune.children.clear();
			}
		}
		while (_Prune!=null);

		return root;//now this node is pruned, return it
	}

	private double Accuracy(List<Instance> instance,DecTreeNode root)
	{
		if (instance.size()==0) return 0;
		int count=0;
		for (Instance x:instance)
			if (classify(x,root)==x.label) count++;
		return (double) count/instance.size();
	}


	private int classify(Instance instance,DecTreeNode root)
	{
		DecTreeNode temp=root;
		while (!temp.terminal)//search the tree while current node is not terminal node
		{
			for (DecTreeNode child:temp.children)
				if (child.parentAttributeValue==instance.attributes.get(temp.attribute)) 
					{
						temp=child;
						break;
					}
		}
		return temp.label;
	}
	private double Entropy(List<Instance> instance,int labels_num)
	{
		int[] count=new int[labels_num];
		double P;
		double total_Entropy=0.0;

		for (Instance x:instance)
			count[x.label]++;
		for (int x:count)
			if (x!=0)
			{
				P=(double)x/instance.size();
				total_Entropy-=P*Math.log(P);
			}
		return total_Entropy/Math.log(2);
	}
	private double Entropy(List<Instance> instance,int dimensions,int labels_num,int feature_num)
	{
		int[][] count=new int[feature_num][labels_num];
		int[]   total=new int[feature_num];

		double entropy;
		double P;
		double total_entropy=0.0;
	
		for (Instance x:instance)
		{
			count[x.attributes.get(dimensions)][x.label]++;
			total[x.attributes.get(dimensions)]++;
		}
		for (int i=0;i<feature_num;i++)
		{
			entropy=0.0;
			for (int x:count[i])
				if (x!=0) 
				{
					P=(double)x/total[i];
					entropy-=P*Math.log(P);
				}
			total_entropy+=((double)total[i]/instance.size())*entropy;
		}
		return total_entropy/Math.log(2);
	}
}
